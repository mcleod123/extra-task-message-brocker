package com.itea.kazmin;

import java.util.Date;
import java.util.LinkedList;

public class MyMessageBrocker {

    LinkedList<String> someObjectsList = new LinkedList<>();
    final int OBJECT_LIST_INITIAL_SIZE = 2;
    final int PRODUCE_INTERVAL = 1000;
    final int READ_INTERVAL = 100;

    public void ProduceMessage() throws InterruptedException {
        String producerMessage;
        Date currentDate;

        while (true) {

            synchronized (this) {
                while (someObjectsList.size() == OBJECT_LIST_INITIAL_SIZE) {
                    wait();
                }

                currentDate = new Date();

                producerMessage = currentDate + " - Some message from producer";

                System.out.println("-> Producer produced message: "
                        + producerMessage);
                someObjectsList.add(producerMessage);

                notify();

                Thread.sleep(PRODUCE_INTERVAL);
            }
        }
    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (this) {
                while (someObjectsList.size() == 0) {
                    wait();
                    System.out.println("[!] Consumer Wait for new Message Elements...");
                }


                String removeFirst = someObjectsList.removeFirst();

                System.out.println("<- Consumer consumed message: "
                        + removeFirst);
                notify();
                Thread.sleep(READ_INTERVAL);
            }
        }
    }
}