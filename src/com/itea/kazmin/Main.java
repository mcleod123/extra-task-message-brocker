package com.itea.kazmin;

public class Main {

    public static void main(String[] args) {

        InitApplication();

    }


    public static void InitApplication() {

        final MyMessageBrocker myMessageBrocker = new MyMessageBrocker();

        Thread ProducerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    myMessageBrocker.ProduceMessage();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread ConsumerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    myMessageBrocker.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        ProducerThread.start();
        ConsumerThread.start();

        try {
            ProducerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            ConsumerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}